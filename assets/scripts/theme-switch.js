// Select the button
const darkModeToggle = document.querySelector(".tgl");

darkModeToggle.addEventListener("click", function () {
  document.body.classList.toggle("dark-mode");
});
